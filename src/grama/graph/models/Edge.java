package grama.graph.models;

import java.util.Objects;

public class Edge {
    private char type;
    private int length;

    private Node node1;
    private Node node2;

    private static int lastId = 0;
    private final int id;

    /**
     * An edge is a link between two nodes
     * @param type :char: edge type
     * @param node1 :Node: first node
     * @param node2 :Node: second node
     * @param length :int: lenght (km)
     */
    public Edge(char type, Node node1, Node node2, int length) {
        this.type = type;
        this.node1 = node1;
        this.node2 = node2;
        this.id = lastId ++;
        this.length = length;
    }

    public char getType() {
        return type;
    }

    public Node getNode1() {
        return node1;
    }

    public Node getNode2() {
        return node2;
    }

    public int getLength() {
        return length;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Edge other = (Edge) obj;
        if (this.type != other.getType()) {
            return false;
        }
        if (this.length != other.getLength()) {
            return false;
        }

        if (Objects.equals(this.node1, other.getNode1()) && Objects.equals(this.node2, other.getNode2())){
            return true;
        }

        return Objects.equals(this.node1, other.getNode2()) && Objects.equals(this.node2, other.getNode1());
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, length);
    }

    @Override
    public String toString(){
        return this.node1.getName() + ":" + this.type+","+this.length+"::"+this.node2.getName();
    }
}
