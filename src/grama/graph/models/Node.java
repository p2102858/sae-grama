package grama.graph.models;

import grama.graph.Graph;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Node{

    public static final int DEFAULT_DIAMETER = 50;

    private final String name;
    private final char type;
    private final List<Edge> edges;
    private final Graph graph;
    private boolean selected;
    private boolean hovered;
    private int diameter;
    private int degree = Integer.MAX_VALUE;

    private int x;
    private int y;

    /**
     * A node is a point of the graph
     * @param name :String: the node's name (e.g : Lyon)
     * @param type :char: the node's type (V/R/L)
     * @param graph :Graph: the graph containing the node
     */
    public Node(String name, char type, Graph graph) {
        this.name = name;
        this.type = type;
        this.edges = new ArrayList<>();
        this.graph = graph;
        this.diameter = DEFAULT_DIAMETER;
    }



    public String getName() {
        return name;
    }

    public char getType() {
        return type;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isSelected(){return this.selected;}

    public void setSelected(boolean selected){this.selected = selected;}

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public boolean isHovered() {
        return hovered;
    }

    public void setHovered(boolean hovered) {
        this.hovered = hovered;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public void addEdge(Edge edge){
        //adds an edge to the node's array list
        if(!this.edges.contains(edge)){
            //the edge is not present in the node's array list
            //create a copy of the new edge going in the opposite direction
            Edge edge2 = new Edge( edge.getType(),edge.getNode2(), edge.getNode1(),edge.getLength());
            //add both edges to the grama.graph's edges array list
            this.graph.addEdge(edge);
            //add the copy of the edge to the target node's edges array list
            edge.getNode2().getEdges().add(edge2);
            //add the edge to node's array list
            this.edges.add(edge);
        }
    }

    public boolean removeEdge(Edge edge)  {
        //removes an edge to the node's array list
        if(this.edges.contains(edge)){
            //the edge is present in the node's array list
            //create a copy of the new edge going in the opposite direction
            Edge edge2 = new Edge(edge.getType(),edge.getNode2(), edge.getNode1(),edge.getLength());
            //remove the edges to the grama.graph's edges array list
            this.graph.removeEdge(edge);
            //remove the copy of the deleted edge from the target node's edges array list
            edge.getNode2().getEdges().remove(edge2);
            //remove the edge of the node's edges array list
            return this.edges.remove(edge);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return type == node.type && Objects.equals(name, node.name);
    }

    @Override
    public String toString() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(this.type).append(",").append(this.name).append(" ");
        strBuilder.append('\n');
        strBuilder.append("Edges : ");
        strBuilder.append('\n');
        for(Edge edge : this.edges){
            strBuilder.append(edge.getType()).append(",").append(edge.getLength()).append("::").append(edge.getNode2().getType()).append(",").append(edge.getNode2().getName()).append('\n');
        }
        strBuilder.append("---------------");
        return strBuilder.toString();
    }
}
