package grama.graph.exceptions;

public class InvalidTypeException extends Exception{
    private final char invalidType;
    private final char type1;
    private final char type2;
    private final String depNode;
    private final String arrNode;

    public InvalidTypeException(char invalidType,char type1,char type2, String depNode, String arrNode) {
        this.invalidType = invalidType;
        this.depNode = depNode;
        this.arrNode = arrNode;
        this.type1 = type1;
        this.type2 = type2;
    }

    public char getInvalidType() {
        return invalidType;
    }

    public char getType1() {
        return type1;
    }

    public char getType2() {
        return type2;
    }

    public String getDepNode() {
        return depNode;
    }

    public String getArrNode() {
        return arrNode;
    }
}
