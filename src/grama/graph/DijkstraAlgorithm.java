package grama.graph;

import grama.graph.models.Edge;
import grama.graph.models.Node;

import java.util.*;

public class DijkstraAlgorithm {

    public static List<Node> execute(Graph graph, Node source, Node dest) {
        ArrayList<Node> unsettledNodes = new ArrayList<>(graph.getNodes());
        ArrayList<Node> settledNodes = new ArrayList<>();
        Node currentNode = source;
        currentNode.setDegree(0);
        Node precNode = null;
        unsettledNodes.remove(source);
        settledNodes.add(source);
        while(unsettledNodes.size()!=0){
            for(Edge edge : source.getEdges()){
                precNode = currentNode;
                currentNode = edge.getNode2();
                currentNode.setDegree(edge.getLength()+precNode.getDegree());
                settledNodes.add(currentNode);
                unsettledNodes.remove(currentNode);

            }
        }

        return null;
    }
}
