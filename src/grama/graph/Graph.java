package grama.graph;

import grama.assets.AssetGetter;
import grama.graph.exceptions.InvalidTypeException;
import grama.graph.models.Edge;
import grama.graph.models.Node;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Graph implements Cloneable {
    private ArrayList<Node> nodes;
    private ArrayList<Edge> edges;
    private final String defaultPath;
    private String currentPath;

    /**
     * A graph is composed of Nodes and Edges
     * This class also contains all the methods to analyse the nodes (closestNeighbour, isMoreOpen...)
     */
    public Graph() {
        String defaultPath1;
        this.nodes = new ArrayList<>();
        this.edges = new ArrayList<>();
        defaultPath1 = AssetGetter.getGraphPath("Graph.csv");
        this.defaultPath = defaultPath1;
        this.currentPath = defaultPath;
    }

    public void readGraph(String path) throws InvalidTypeException {
        try {
            ArrayList<Node> nodesBackup = nodes;
            ArrayList<Edge> edgesBackup = edges;
            nodesBackup.addAll(nodes);
            edgesBackup.addAll(edges);
            this.nodes.clear();
            this.edges.clear();
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.equals("")) {
                    String[] tokens = line.split(":");
                    try {
                        String node1Content = tokens[0];
                        String edgeContent = tokens[1];
                        String node2Content = tokens[3];

                        Node node1 = new Node(node1Content.substring(2), node1Content.charAt(0), this);
                        Node node2 = new Node(node2Content.substring(2,node2Content.length()-1), node2Content.charAt(0), this);

                        if ((node1.getType() != 'R' && node1.getType() != 'L' && node1.getType() != 'V')) {
                            throw new InvalidTypeException(node1.getType(), node1.getType(), node2.getType(), node1.getName(), node2.getName());
                        }
                        if ((node2.getType() != 'R' && node2.getType() != 'L' && node2.getType() != 'V')) {
                            throw new InvalidTypeException(node2.getType(), node2.getType(), node1.getType(), node2.getName(), node1.getName());
                        }
                        if (this.nodes.contains(node1)) {
                            try {
                                node1 = this.nodes.get(this.nodes.indexOf(node1));
                            } catch (IndexOutOfBoundsException e) {
                                System.out.println(node1);
                                System.out.println(this.nodes.indexOf(node1));
                            }
                        }
                        if (this.nodes.contains(node2)) {
                            try {
                                node2 = this.nodes.get(this.nodes.indexOf(node2));
                            } catch (IndexOutOfBoundsException e) {
                                System.out.println(node2);
                                System.out.println(this.nodes.indexOf(node2));
                            }
                        }
                        Edge edge = new Edge(edgeContent.charAt(0), node1, node2,Integer.parseInt(edgeContent.substring(2)));
                        node1.addEdge(edge);
                        this.addNode(node1);
                        this.addNode(node2);
                    }
                    catch(ArrayIndexOutOfBoundsException e){
                        e.printStackTrace();
                        nodes.addAll(nodesBackup);
                        edges.addAll(edgesBackup);
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public void setNodes(ArrayList<Node> nodes){this.nodes = nodes;}

    public ArrayList<Edge> getEdges() {
        return edges;
    }

    public void setEdges(ArrayList<Edge> edges){this.edges = edges;}

    public String getDefaultPath() {
        return defaultPath;
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public void setCurrentPath(String currentPath) {
        this.currentPath = currentPath;
    }


    public void addEdge(Edge edge) {
        if (!this.edges.contains(edge)) {
            edges.add(edge);
        }
    }

    public void removeEdge(Edge edge) {
        if (this.edges.contains(edge)) {
            edges.remove(edge);
        }
    }

    public void addNode(Node node) {
        if (!this.nodes.contains(node)) {
            nodes.add(node);
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public boolean removeNode(Node node) {
        return this.nodes.remove(node);
    }

    public Node closestNeighbour(Node node) {
        // returns the closest neighbour of the specified node
        int shortestLength = 2000;
        Node closestNode = null;
        for (Edge edge : node.getEdges()) {
            if (edge.getLength() < shortestLength) {
                closestNode = edge.getNode2();
                shortestLength = edge.getLength();
            }
        }
        return closestNode;
    }

    public Node closestNeighbour(Node node, List<Node> nodesToExclude) {
        // returns the closest neighbour of the specified node
        int shortestLength = 2000;
        Node closestNode = null;
        for (Edge edge : node.getEdges()) {
            if (edge.getLength() < shortestLength && !nodesToExclude.contains(edge.getNode2())) {
                closestNode = edge.getNode2();
                shortestLength = edge.getLength();
            }
        }
        return closestNode;
    }

    public ArrayList<Node> directNeighbours(Node node) {
        // returns all neighbours 1 jump away from the node
        ArrayList<Node> neighbours = new ArrayList<>();
        for (Edge edge : node.getEdges()) {
            neighbours.add(edge.getNode2());
        }
        return neighbours;
    }

    public ArrayList<Node> neighbourCities(Node node) {
        // returns all neighbours 1 jump away from the node
        ArrayList<Node> cities = new ArrayList<>();
        for (Edge edge : node.getEdges()) {
            if (edge.getNode2().getType() == 'V') {
                cities.add(edge.getNode2());
            }
        }
        return cities;
    }

    public boolean twoJumpsAwayTypeNodes(Node node1, Node node2) {
        //returns true if the nodes are 2 jumps away
        for (Edge edge1 : node1.getEdges()) {
            //goes through the edges of node 1
            for (Edge edge2 : edge1.getNode2().getEdges()) {
                //goes through edges of all of node1 direct neighbours
                if (edge2.getNode2().equals(node2) && edge2.getNode2() != node1) {
                    //a 2 length path from node1 to node2 has been found
                    return true;
                }
            }
        }
        //there is no 2 length path from node1 to node2
        return false;
    }

    public ArrayList<Node> getCities() {
        // returns an array list of all the cities
        ArrayList<Node> cities = new ArrayList<>();
        for (Node node : nodes) {
            if (node.getType() == 'V') {
                cities.add(node);
            }
        }
        return cities;
    }

    public ArrayList<Node> getRestaurants() {
        // returns an array list of all the restaurants
        ArrayList<Node> restaurants = new ArrayList<>();
        for (Node node : nodes) {
            if (node.getType() == 'R') {
                restaurants.add(node);
            }
        }
        return restaurants;
    }

    public ArrayList<Node> getLeisurePlaces() {
        //returns an array list of all the leisure places
        ArrayList<Node> leisurePlaces = new ArrayList<>();
        for (Node node : nodes) {
            if (node.getType() == 'L') {
                leisurePlaces.add(node);
            }
        }
        return leisurePlaces;
    }

    public ArrayList<Edge> getHighways() {
        // returns an array list of all the highways
        ArrayList<Edge> roads = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getType() == 'A') {
                roads.add(edge);
            }
        }
        return roads;
    }

    public ArrayList<Edge> getNationalRoads() {
        // returns an array list of all the nationals roads
        ArrayList<Edge> roads = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getType() == 'N') {
                roads.add(edge);
            }
        }
        return roads;
    }

    public ArrayList<Edge> getDepartmentalRoads() {
        // returns an array list of all the departmental roads
        ArrayList<Edge> roads = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getType() == 'D') {
                roads.add(edge);
            }
        }
        return roads;
    }

    public int twoJumpsAwayTypeNodes(Node node1, char type) {
        int i = 0;
        //returns the number of 2 length paths from node1 and a node of the specified type
        for (Edge edge1 : node1.getEdges()) {
            //goes through all the edges
            for (Edge edge2 : edge1.getNode2().getEdges()) {
                //goes through all the edges again
                if (edge2.getNode2().getType() == type && edge2.getNode2() != node1) {
                    i++;
                }
            }
        }
        return i;
    }

    public boolean isMoreOpen(Node node1, Node node2) {
        // returns true if there are more cities 2 jumps away from node1 than from node2
        return twoJumpsAwayTypeNodes(node1, 'V') > twoJumpsAwayTypeNodes(node2, 'V');
    }

    public boolean isMoreGastronomical(Node node1, Node node2) {
        // returns true if there are more restaurants 2 jumps away from node1 than from node2
        return twoJumpsAwayTypeNodes(node1, 'R') > twoJumpsAwayTypeNodes(node2, 'R');
    }

    public boolean isMoreCultural(Node node1, Node node2) {
        // returns true if there are more cultural locations 2 jumps away from node1 than from node2
        return twoJumpsAwayTypeNodes(node1, 'L') > twoJumpsAwayTypeNodes(node2, 'L');
    }

    public Graph createSubGraph(char[] nodeTypes){
        Graph subGraph = new Graph();
        for(char nodeType : nodeTypes){
            switch (nodeType){
                case('V') -> {
                    subGraph.getNodes().addAll(this.getCities());
                    for(Edge edge : this.edges){
                        if(Arrays.toString(nodeTypes).contains(Character.toString(edge.getNode1().getType()))
                           && Arrays.toString(nodeTypes).contains(Character.toString(edge.getNode2().getType()))){
                            subGraph.addEdge(edge);
                        }
                    }
                }
                case('R') -> {
                    subGraph.getNodes().addAll(this.getRestaurants());
                    for(Edge edge : this.edges){
                        if(edge.getNode1().getType() == 'R' || edge.getNode2().getType() == 'R'){
                            subGraph.addEdge(edge);
                        }
                    }
                }
                case('L') -> {
                    subGraph.getNodes().addAll(this.getLeisurePlaces());
                    for(Edge edge : this.edges){
                        if(edge.getNode1().getType() == 'L' || edge.getNode2().getType() == 'L'){
                            subGraph.addEdge(edge);
                        }
                    }
                }
            }
        }
        return subGraph;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Graph graph = (Graph) o;
        return Objects.equals(nodes, graph.nodes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodes) + 6554;
    }

}
