package grama.gui.components.frames;

import grama.graph.models.Edge;
import grama.graph.models.Node;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;

public class NodesEdgesList extends JFrame {

    /**
     * Displays a list of the nodes and edges
     * @param frame parent frame
     */
    public NodesEdgesList(MainFrame frame){
        this.frame = frame;
        this.initComponent();
        this.setText(frame.getGraphPanel().getGraph().getNodes(),frame.getGraphPanel().getGraph().getEdges());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(500,500);
        setLocationRelativeTo(frame);
        setVisible(true);
    }

    private void initComponent(){
        this.setLayout(new BorderLayout());
        this.textArea = new JTextArea();
        this.scrollPane = new JScrollPane();
        this.bottomPanel = new JPanel();
        this.close = new JButton("Fermer");
        textArea.setPreferredSize(new Dimension(this.getWidth(),(int)(this.getHeight()*0.8)));
        textArea.setColumns(20);
        textArea.setRows(5);
        textArea.setEditable(false);
        close.addActionListener(e-> this.dispose());
        scrollPane.setViewportView(textArea);
        bottomPanel.setSize(this.getWidth(),(int)(this.getHeight()*0.2));
        getContentPane().add(scrollPane,BorderLayout.CENTER);
        bottomPanel.setLayout(new FlowLayout());
        bottomPanel.add(close);
        add(bottomPanel,BorderLayout.SOUTH);
    }

    private void setText(List<Node> nodes, List<Edge> edges){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("--Noeuds--"+'\n');
        for(Node node : nodes) {
            stringBuilder.append(node.getType()).append(",").append(node.getName()).append('\n');
        }
        stringBuilder.append("--Arrêtes--"+'\n');
        for(Edge edge : edges) {
            stringBuilder.append(edge.getType()).append(",").append(edge.getLength()).append('\n');
        }
        this.textArea.setText(stringBuilder.toString());
    }

    private JTextArea textArea;
    private JScrollPane scrollPane;
    private JPanel bottomPanel;
    private JButton close;
    private final MainFrame frame;
}
