package grama.gui.components.frames;

import grama.Engine;
import grama.graph.Graph;
import grama.graph.models.Node;
import grama.gui.components.menuitems.file.OpenDefaultGraphMenuItem;
import grama.gui.components.menuitems.file.OpenFileMenuItem;
import grama.gui.components.menuitems.file.ReloadGraphMenuItem;
import grama.gui.components.panels.GraphDisplayPanel;
import grama.gui.components.panels.MenuPanel;
import grama.interfaces.NodeMouseListener;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JSeparator;

import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.event.MouseEvent;

public class MainFrame extends JFrame implements NodeMouseListener {

    public static final int GAP = 30;
    private Engine engine;

    /**
     * Main frame; contians the menu panel and the graph display panel
     * @param graph graph do display
     * @param xMax width
     * @param yMax height
     */
    public MainFrame(Graph graph, int xMax, int yMax) {
        setTitle("Grama");
        setBackground(Color.WHITE);
        setSize(xMax, yMax);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        initComponents(graph);
    }

    private void initComponents(Graph graph) {

        bar = new JMenuBar();

        fileMenu = new JMenu();
        viewMenu = new JMenu();
        fileMenu.setText("File");
        viewMenu.setText("View");

        OpenFileMenuItem openFileMenuItem = new OpenFileMenuItem(this, graph);
        OpenDefaultGraphMenuItem openDefaultGraphMenuItem = new OpenDefaultGraphMenuItem(this, graph);
        reloadGraphMenuItem = new ReloadGraphMenuItem(graph, this);


        graphPanel = new GraphDisplayPanel(graph, this);
        menuPanel = new MenuPanel(this);

        fileMenu.add(openFileMenuItem);
        fileMenu.add(openDefaultGraphMenuItem);
        fileMenu.add(new JSeparator());
        fileMenu.add(reloadGraphMenuItem);


        add(graphPanel, BorderLayout.CENTER);
        add(menuPanel, BorderLayout.WEST);
        setJMenuBar(bar);

        bar.add(fileMenu);
        bar.add(viewMenu);

        this.engine = new Engine(graphPanel);
        this.engine.setListener(this);

    }

    public void nodeClicked(MouseEvent e, Node nodeClicked) {
        if (!nodeClicked.isSelected() && GraphDisplayPanel.nodesSelected < 2) {
            // set the node to selected and increase the selected nodes counter
            nodeClicked.setSelected(true);
            GraphDisplayPanel.nodesSelected++;
        } else if (nodeClicked.isSelected()) {
            //set the node to unselected, decrease the selected nodes counter and remove it's direct neighbours from the nodes to highlight list
            nodeClicked.setSelected(false);
            GraphDisplayPanel.nodesSelected--;
            getGraphPanel().getNodesToHighlight().removeAll(getGraphPanel().getGraph().directNeighbours(nodeClicked));
            repaint();
        }

        if (GraphDisplayPanel.nodesSelected == 0) {
            // update the menu panel
            menuPanel.noNodeSelected();
        } else if (GraphDisplayPanel.nodesSelected == 1) {
            // update the menu panel
            menuPanel.oneNodeSelected(nodeClicked);
        } else if (GraphDisplayPanel.nodesSelected == 2) {
            Node[] nodes = new Node[2];
            int i = 0;
            // find the selected nodes
            for (Node node : graphPanel.getNodesToDisplay()) {
                if (node.isSelected()) {
                    nodes[i] = node;
                    i++;
                }
            }
            // update the menu panel
            menuPanel.twoNodesSelected(nodes[0], nodes[1]);
        }
        repaint();
        menuPanel.revalidate();
        menuPanel.repaint();
    }

    public void nodeDragged(MouseEvent e, Node nodeDragged) {
        if(GraphDisplayPanel.nodesDragged == 0) {
            GraphDisplayPanel.nodesDragged = 1;
            int x = e.getX();
            int y = e.getY();
            nodeDragged.setX(x - nodeDragged.getDiameter() / 2);
            nodeDragged.setY(y - nodeDragged.getDiameter() / 2);
        }
        graphPanel.repaint();
    }

    public void nodeEntered(MouseEvent e, Node nodeEntered) {
        if (!nodeEntered.isHovered() && GraphDisplayPanel.nodesHovered == 0) {
            GraphDisplayPanel.nodesHovered = 1;
            nodeEntered.setHovered(true);
            this.engine.setHoveredNode(nodeEntered);
        }
        repaint();
    }

    public GraphDisplayPanel getGraphPanel() {
        return graphPanel;
    }

    public MenuPanel getMenuPanel() {
        return menuPanel;
    }

    public JMenu getViewMenu() {
        return viewMenu;
    }

    public void setViewMenu(JMenu viewMenu) {
        this.viewMenu = viewMenu;
    }

    public JMenu getFileMenu() {
        return fileMenu;
    }

    public void setFileMenu(JMenu fileMenu) {
        this.fileMenu = fileMenu;
    }

    public JMenuBar getBar() {
        return bar;
    }

    public void setBar(JMenuBar bar) {
        this.bar = bar;
    }

    public void setMenuPanel(MenuPanel menuPanel) {
        this.menuPanel = menuPanel;
    }

    public ReloadGraphMenuItem getReloadGraphMenuItem() {
        return reloadGraphMenuItem;
    }

    private JMenu viewMenu;
    private JMenu fileMenu;
    private JMenuBar bar;
    private MenuPanel menuPanel;
    private GraphDisplayPanel graphPanel;
    private ReloadGraphMenuItem reloadGraphMenuItem;
}
