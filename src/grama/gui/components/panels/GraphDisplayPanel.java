package grama.gui.components.panels;

import grama.graph.models.Edge;
import grama.graph.Graph;
import grama.graph.models.Node;
import grama.gui.components.frames.MainFrame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Font;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GraphDisplayPanel extends JPanel {

    private Graph graph;
    private final Graph defaultGraph;

    private final List<Edge> paintedEdges;
    private final List<Node> nodesToDisplay;
    private final List<Node> maskedNodes;
    private List<Node> nodesToHighlight;

    private boolean displayCities = true;
    private boolean displayRestaurents = true;
    private boolean displayLeisurePlaces = true;

    private boolean displayHighways = true;
    private boolean displayNationals = true;
    private boolean displayDepartmentals = true;

    private final Color citiesColor = new Color(0, 100, 255);
    private final Color restaurantsColor = new Color(255, 192, 0);
    private final Color leisureColor = new Color(189, 3, 255);

    public static int nodesSelected = 0;
    public static int nodesHovered = 0;
    public static int nodesDragged = 0;

    private String edgesTypes;

    /**
     * Display the graph
     * @param graph graph to display
     * @param frame parent frame
     */
    public GraphDisplayPanel(Graph graph, MainFrame frame) {
        this.graph = graph;
        this.defaultGraph = graph;
        this.paintedEdges = new ArrayList<>();
        this.nodesToDisplay = new ArrayList<>();
        this.maskedNodes = new ArrayList<>();
        this.nodesToHighlight = new ArrayList<>();

        this.setPreferredSize(new Dimension((int) (frame.getWidth() * 0.8), (int) (frame.getHeight() * 0.8)));
        setCoordsRandom(graph.getNodes(), frame);
    }


    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        nodesToDisplay.clear();

        // Apply an Antialiasing effect on the panel
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        this.edgesTypes = "";
        // fill up the nodesToDisplay ArrayList according to the state of the checkbox
        if (displayCities) nodesToDisplay.addAll(graph.getCities());
        if (displayRestaurents) nodesToDisplay.addAll(graph.getRestaurants());
        if (displayLeisurePlaces) nodesToDisplay.addAll(graph.getLeisurePlaces());
        // set the edgeTypes string according to the states of teh checkboxes
        if (displayHighways) this.edgesTypes += "A";
        if (displayNationals) this.edgesTypes += "N";
        if (displayDepartmentals) this.edgesTypes += "D";
        // paint the nodes
        for (Node node : nodesToDisplay) if (!maskedNodes.contains(node)) paintNode(node, g);
        // clear the paintedEdges list
        paintedEdges.clear();
    }


    private void paintNode(Node node, Graphics g) {
        // go through the node's edges
        for (Edge edge : node.getEdges()) {
            if (nodesToDisplay.contains(edge.getNode2())) {
                // check if the edge has already been painted
                if (!paintedEdges.contains(edge) && this.edgesTypes.contains(Character.toString(edge.getType()))) {
                    // paint teh edge
                    g.drawLine(edge.getNode1().getX() + node.getDiameter() / 2, edge.getNode1().getY() + node.getDiameter() / 2, edge.getNode2().getX() + edge.getNode2().getDiameter() / 2, edge.getNode2().getY() + edge.getNode2().getDiameter() / 2);
                    // add it to the painted edges list to avoid painting it again
                    paintedEdges.add(edge);
                }
            }
        }

        if (node.isSelected()) {
            // draw the selected nodes in red
            g.setColor(Color.red);
        } else if (nodesToHighlight != null && nodesToHighlight.contains(node)) {
            // nodes to highlight : direct neighbours
            g.setColor(Color.GREEN);
        } else {
            if (node.getType() == 'V') {
                // paint the cities in blue
                g.setColor(citiesColor);
            } else if (node.getType() == 'R') {
                // paint the restaurants in orange
                g.setColor(restaurantsColor);
            } else {
                // paint the leisure places in purple
                g.setColor(leisureColor);
            }
        }
        if (node.isHovered()) {
            // increase the diameter of hovered nodes
            node.setDiameter(Node.DEFAULT_DIAMETER + 10);
        } else {
            // set the diameter back to normal when it's not hovered anymore
            node.setDiameter(Node.DEFAULT_DIAMETER);
        }
        // draw the inside of the node
        g.fillOval(node.getX(), node.getY(), node.getDiameter(), node.getDiameter());
        if (node.isHovered() || node.isSelected()) {
            // set the text size to 20 for the selected and hovered nodes
            g.setFont(new Font("segoe ui", Font.BOLD, 20));
            g.setColor(Color.black);
        } else {
            // set it to 10 if the other nodes
            g.setFont(new Font("segoe ui", Font.PLAIN, 10));
        }
        // draw the node's name
        int stringWidth = g.getFontMetrics().stringWidth(node.getName());
        int stringHeight = g.getFontMetrics().getHeight();
        int x = node.getX() + (node.getDiameter()/2 )- (stringWidth/2);
        int y = node.getY() - stringHeight/2;
        g.drawString(node.getName(), x, y);
        g.setColor(Color.black);
        // draw the outside of the node
        g.drawOval(node.getX(), node.getY(), node.getDiameter(), node.getDiameter());
        // set the text to 20 for the node's name
        g.setFont(new Font("segoe ui", Font.BOLD, 20));
        // draw the node's type

        stringWidth = g.getFontMetrics().stringWidth(Integer.toString(node.getType()));
        stringHeight = g.getFontMetrics().getHeight();
        x = node.getX() + node.getDiameter()/2 - stringWidth/2;
        y = node.getY() + node.getDiameter()/2 + 10;
        g.drawString(Character.toString(node.getType()), x, y);
    }
    public void setCoordsRandom(List<Node> nodes, MainFrame frame) {
        // this method generates random coordinates for the nodes
        Random rand = new Random();
        List<Node> placedNodes = new ArrayList<>();
        // get the bounds of the coordinates
        int xMax = (int) (frame.getWidth() * 0.8);
        int yMax = (int) (frame.getHeight() * 0.8);

        for (Node node : graph.getNodes()) {
            // generate the coordinates
            generateCoords(node,rand,xMax,yMax);
        }
        for (Node node1 : nodes) {
            checkNodesDistance(node1,placedNodes,xMax,yMax,rand);
            placedNodes.add(node1);
        }
    }

    private int calculateDistance(Node node1, Node node2) {
        // this method returns the distance between node1 and node2
        return (int) Math.sqrt((Math.pow(node1.getX() - node2.getX(), 2)) + (Math.pow(node1.getY() - node2.getY(), 2)));
    }

    private void checkNodesDistance(Node node1, List<Node> placedNodes, int xMax, int yMax, Random rand) {
        // this method set the coordinates of node1 to random numbers until it's far enough from node2
        for(int i = 0; i < placedNodes.size();i++) {
            Node node2 = placedNodes.get(i);
            while (calculateDistance(node1, node2) < Node.DEFAULT_DIAMETER / 2) {
                generateCoords(node1, rand, xMax, yMax);
                i = 0;
            }
        }
    }

    private void generateCoords(Node node, Random rand, int xMax, int yMax) {
        node.setX(rand.nextInt(xMax - Node.DEFAULT_DIAMETER + 10));
        node.setY(rand.nextInt(yMax - Node.DEFAULT_DIAMETER + 10));
    }

    public List<Node> getMaskedNodes() {
        return maskedNodes;
    }

    public void unselectAllNodes() {
        for (Node node : graph.getNodes()) {
            if (node.isSelected()) {
                node.setSelected(false);
            }
            nodesSelected = 0;
        }
    }

    public boolean isDisplayCities() {
        return displayCities;
    }

    public void setDisplayCities(boolean displayCities) {
        this.displayCities = displayCities;
    }

    public boolean isDisplayRestaurents() {
        return displayRestaurents;
    }

    public void setDisplayRestaurents(boolean displayRestaurents) {
        this.displayRestaurents = displayRestaurents;
    }

    public boolean isDisplayLeisurePlaces() {
        return displayLeisurePlaces;
    }

    public void setDisplayLeisurePlaces(boolean displayLeisurePlaces) {
        this.displayLeisurePlaces = displayLeisurePlaces;
    }

    public boolean isDisplayHighways() {
        return displayHighways;
    }

    public void setDisplayHighways(boolean displayHighways) {
        this.displayHighways = displayHighways;
    }

    public boolean isDisplayNationals() {
        return displayNationals;
    }

    public void setDisplayNationals(boolean displayNationals) {
        this.displayNationals = displayNationals;
    }

    public boolean isDisplayDepartmentals() {
        return displayDepartmentals;
    }

    public void setDisplayDepartmentals(boolean displayDepartmentals) {
        this.displayDepartmentals = displayDepartmentals;
    }

    public List<Node> getNodesToDisplay() {
        return this.nodesToDisplay;
    }

    public List<Node> getNodesToHighlight() {
        return nodesToHighlight;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public Graph getGraph() {
        return graph;
    }
}
