package grama.gui.components.panels;

import grama.graph.models.Node;
import grama.gui.checkboxes.edges.DisplayDepartmentalsCheckbox;
import grama.gui.checkboxes.edges.DisplayHighwaysCheckbox;
import grama.gui.checkboxes.edges.DisplayNationalsCheckbox;
import grama.gui.checkboxes.nodes.DisplayRestaurantsCheckbox;
import grama.gui.checkboxes.nodes.DisplayCitiesCheckbox;
import grama.gui.checkboxes.nodes.DisplayLeisureCheckbox;
import grama.gui.components.buttons.RefreshFrameButton;
import grama.gui.components.frames.MainFrame;
import grama.gui.components.frames.NodesEdgesList;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JSeparator;

import java.awt.Font;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Dimension;

public class MenuPanel extends JPanel {

    private final MainFrame frame;

    /**
     * Menu panel
     * @param frame parent frame
     */
    public MenuPanel(MainFrame frame) {
        this.frame = frame;

        setBackground(Color.LIGHT_GRAY);
        setLayout(new FlowLayout());

        setSize(new Dimension((int) (frame.getWidth() * 0.2), (int) (frame.getHeight() * 0.2)));
        setPreferredSize(new Dimension((int) (frame.getWidth() * 0.2), (int) (frame.getHeight() * 0.2)));
        setMaximumSize(new Dimension((int) (frame.getWidth() * 0.2), (int) (frame.getHeight() * 0.2)));
        setMinimumSize(new Dimension((int) (frame.getWidth() * 0.2), (int) (frame.getHeight() * 0.2)));
        setVisible(true);
        initComponents();
        noNodeSelected();
    }

    private void initComponents() {
        this.menu = new JLabel("Menu");
        this.displayRestaurantsCheckbox = new DisplayRestaurantsCheckbox(frame);
        this.displayCitiesCheckbox = new DisplayCitiesCheckbox(frame);
        this.displayLeisureCheckbox = new DisplayLeisureCheckbox(frame);
        this.displayHighwaysCheckbox = new DisplayHighwaysCheckbox(frame);
        this.displayNationalsCheckbox = new DisplayNationalsCheckbox(frame);
        this.displayDepartmentalsCheckbox = new DisplayDepartmentalsCheckbox(frame);
        this.nodeSelected = new JLabel();
        this.maskSelectedNode = new JCheckBox();
        this.displayAllMaskedNodes = new JButton("Afficher les noeuds masqués");
        this.displayAllMaskedNodes.addActionListener(e -> {
            frame.getGraphPanel().getMaskedNodes().clear();
            maskSelectedNode.setSelected(false);
            frame.repaint();
        });
        this.refreshFrameButton = new RefreshFrameButton(frame);
        this.displayShortestPath = new JButton("Afficher le chemin le plus court");
        this.nodesAreTwoJumpAway = new JLabel();
        this.isMoreOpen = new JLabel();
        this.isMoreGastronomic = new JLabel();
        this.isMoreCultural = new JLabel();
        this.unselectAllNodes = new JButton("Déselectionner tous les noeuds");
        unselectAllNodes.addActionListener(e -> {
            frame.getGraphPanel().unselectAllNodes();
            frame.getGraphPanel().getNodesToHighlight().clear();
            noNodeSelected();
        });
        this.numberOfTypeNodes = new JLabel(frame.getGraphPanel().getGraph().getNodes().size() + " noeuds, " + frame.getGraphPanel().getGraph().getCities().size() + " V, " + frame.getGraphPanel().getGraph().getRestaurants().size() + " R, " + frame.getGraphPanel().getGraph().getLeisurePlaces().size() + " L");
        this.numberOfTypeEdges = new JLabel(frame.getGraphPanel().getGraph().getEdges().size() + " arrêtes, " + frame.getGraphPanel().getGraph().getHighways().size() + " A, " + frame.getGraphPanel().getGraph().getNationalRoads().size() + " N, " + frame.getGraphPanel().getGraph().getDepartmentalRoads().size() + " D");
        this.openList = new JButton();
        this.openList.setText("Afficher la liste des noeuds & arrêtes");
        this.displayDirectNeighbours = new JCheckBox();
        displayDirectNeighbours.setText("Afficher les voisins directs");
        openList.addActionListener(e -> new NodesEdgesList(frame));
        menu.setSize(new Dimension(getWidth(), 20));
        menu.setFont(new Font("Segeo UI", Font.BOLD, 15));
        menu.setAlignmentY(CENTER_ALIGNMENT);
    }

    public void noNodeSelected() {
        // this method adds the components needed for when there are no nodes selected
        removeAll();
        add(menu);
        add(refreshFrameButton);
        add(unselectAllNodes);
        add(displayAllMaskedNodes);
        add(new JSeparator());
        add(openList);
        add(displayCitiesCheckbox);
        add(displayRestaurantsCheckbox);
        add(displayLeisureCheckbox);
        add(displayHighwaysCheckbox);
        add(displayNationalsCheckbox);
        add(displayDepartmentalsCheckbox);
        add(numberOfTypeNodes);
        add(numberOfTypeEdges);
    }

    public void oneNodeSelected(Node selectedNode) {
        // this method adds the components needed for when there is one node selected
        noNodeSelected();
        this.nodeSelected.setText("Le noeud " + selectedNode.getName() + " est selectionné.");
        this.maskSelectedNode.setText("Masquer le noeud " + selectedNode.getName());
        maskSelectedNode.addActionListener(e -> {
            if (maskSelectedNode.isSelected()) {
                frame.getGraphPanel().getMaskedNodes().add(selectedNode);
                selectedNode.setSelected(false);
                GraphDisplayPanel.nodesSelected--;
                frame.repaint();
            } else {
                frame.getGraphPanel().getMaskedNodes().remove(selectedNode);
                frame.repaint();
            }
        });

        displayDirectNeighbours.addActionListener(e -> {
            if(displayDirectNeighbours.isSelected()) {
                frame.getGraphPanel().getNodesToHighlight().addAll(frame.getGraphPanel().getGraph().directNeighbours(selectedNode));
                frame.repaint();
            }
            else{
                frame.getGraphPanel().getNodesToHighlight().removeAll(frame.getGraphPanel().getGraph().directNeighbours(selectedNode));
                frame.repaint();
            }
        });

        add(nodeSelected);
        add(maskSelectedNode);
        add(displayDirectNeighbours);
    }

    public void twoNodesSelected(Node selectedNode1, Node selectedNode2) {
        // this method adds the components needed for when there are two nodes selected
        this.nodeSelected.setText("Les noeuds " + selectedNode1.getName() + " et " + selectedNode2.getName() + " sont selectionnés.");
        this.maskSelectedNode.setText("Masquer les noeuds " + selectedNode1.getName() + ", " + selectedNode2.getName());

        maskSelectedNode.addActionListener(e -> {
            if (maskSelectedNode.isSelected()) {
                frame.getGraphPanel().getMaskedNodes().add(selectedNode1);
                frame.getGraphPanel().getMaskedNodes().add(selectedNode2);
                selectedNode1.setSelected(false);
                selectedNode2.setSelected(false);
                frame.repaint();
            } else {
                frame.getGraphPanel().getMaskedNodes().remove(selectedNode1);
                frame.getGraphPanel().getMaskedNodes().remove(selectedNode2);
                frame.repaint();
            }
        });
        displayDirectNeighbours.addActionListener(e -> {
            if(displayDirectNeighbours.isSelected()) {
                frame.getGraphPanel().getNodesToHighlight().addAll(frame.getGraphPanel().getGraph().directNeighbours(selectedNode1));
                frame.getGraphPanel().getNodesToHighlight().addAll(frame.getGraphPanel().getGraph().directNeighbours(selectedNode2));
                frame.repaint();
            }
            else{
                frame.getGraphPanel().getNodesToHighlight().removeAll(frame.getGraphPanel().getGraph().directNeighbours(selectedNode1));
                frame.getGraphPanel().getNodesToHighlight().removeAll(frame.getGraphPanel().getGraph().directNeighbours(selectedNode2));
                frame.repaint();
            }
        });
        this.displayShortestPath.addActionListener(e -> {
        });

        if (frame.getGraphPanel().getGraph().twoJumpsAwayTypeNodes(selectedNode1, selectedNode2))
            nodesAreTwoJumpAway.setText("les noeuds sont à 2-distance.");
        else nodesAreTwoJumpAway.setText("Les noeuds ne sont pas à 2-distance.");

        if (frame.getGraphPanel().getGraph().isMoreOpen(selectedNode1, selectedNode2))
            isMoreOpen.setText("Le noeud " + selectedNode1.getName() + " est plus ouvert");
        else isMoreOpen.setText("Le noeud " + selectedNode2.getName() + " est plus ouvert");

        if (frame.getGraphPanel().getGraph().isMoreGastronomical(selectedNode1, selectedNode2))
            isMoreGastronomic.setText("Le noeud " + selectedNode1.getName() + " est plus gastronomique");
        else isMoreGastronomic.setText("Le noeud " + selectedNode2.getName() + " est plus gastronomique");

        if (frame.getGraphPanel().getGraph().isMoreCultural(selectedNode1, selectedNode2))
            isMoreCultural.setText("Le noeud " + selectedNode1.getName() + " est plus culturel");
        else isMoreCultural.setText("Le noeud " + selectedNode2.getName() + " est plus culturel");

        add(nodeSelected);
        add(maskSelectedNode);
        add(nodesAreTwoJumpAway);
        add(displayShortestPath);
        add(isMoreOpen);
        add(isMoreGastronomic);
        add(isMoreCultural);
    }

    public DisplayCitiesCheckbox getDisplayCitiesCheckbox() {
        return displayCitiesCheckbox;
    }

    public DisplayRestaurantsCheckbox getDisplayRestaurantsCheckbox() {
        return displayRestaurantsCheckbox;
    }

    public DisplayLeisureCheckbox getDisplayLeisureCheckbox() {
        return displayLeisureCheckbox;
    }

    public JCheckBox getDisplayDirectNeighbours() {
        return displayDirectNeighbours;
    }

    public void setDisplayDirectNeighbours(JCheckBox displayDirectNeighbours) {
        this.displayDirectNeighbours = displayDirectNeighbours;
    }

    private DisplayCitiesCheckbox displayCitiesCheckbox;
    private DisplayRestaurantsCheckbox displayRestaurantsCheckbox;
    private DisplayLeisureCheckbox displayLeisureCheckbox;
    private DisplayHighwaysCheckbox displayHighwaysCheckbox;
    private DisplayDepartmentalsCheckbox displayDepartmentalsCheckbox;
    private DisplayNationalsCheckbox displayNationalsCheckbox;

    private JCheckBox maskSelectedNode;
    private JLabel menu;
    private JLabel nodeSelected;
    private JLabel nodesAreTwoJumpAway;
    private JLabel isMoreOpen;
    private JLabel isMoreGastronomic;
    private JLabel isMoreCultural;
    private JLabel numberOfTypeNodes;
    private JLabel numberOfTypeEdges;
    private RefreshFrameButton refreshFrameButton;
    private JButton unselectAllNodes;
    private JButton displayAllMaskedNodes;
    private JButton displayShortestPath;
    private JButton openList;
    private JCheckBox displayDirectNeighbours;
}
