package grama.gui.components.menuitems.file;

import grama.graph.Graph;
import grama.graph.exceptions.InvalidTypeException;
import grama.gui.components.frames.MainFrame;

import javax.swing.*;
import java.awt.event.KeyEvent;

public class ReloadGraphMenuItem extends JMenuItem {
    /**
     * Reload the graph
     * @param graph displayed graph
     * @param f parent frame
     */
    public ReloadGraphMenuItem(Graph graph, MainFrame f){
        addActionListener(e->{
            try {
                graph.readGraph(graph.getDefaultPath());
            } catch (InvalidTypeException ex) {
                System.out.println("Invalid type found in the grama.graph text file !");
                System.out.println("Type 1 = "+ex.getType1());
                System.out.println("Type 2 = "+ex.getType2());
                System.out.println("Invalid type = "+ex.getInvalidType());
                System.out.println("Name1 = "+ex.getDepNode());
                System.out.println("Name 2 = "+ex.getArrNode());
                System.exit(0);
            }
        });
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,KeyEvent.CTRL_DOWN_MASK));
        setText("Recharger le graphe par défaut");
    }
}
