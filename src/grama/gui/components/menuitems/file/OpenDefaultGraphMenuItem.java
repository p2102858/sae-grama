package grama.gui.components.menuitems.file;

import grama.graph.Graph;
import grama.graph.exceptions.InvalidTypeException;
import grama.gui.components.frames.MainFrame;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class OpenDefaultGraphMenuItem extends JMenuItem {

    private final MainFrame frame;
    private final Graph graph;

    /**
     * Open default file and read the graph
     * @param frame parent frame
     * @param graph displayed graph
     */
    public OpenDefaultGraphMenuItem(MainFrame frame, Graph graph){
        this.frame = frame;
        this.graph = graph;

        setText("Lire le fichier par défault");
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK));
        addActionListener(e->{
            try {
                graph.setCurrentPath(graph.getDefaultPath());
                graph.readGraph(graph.getDefaultPath());
                frame.getGraphPanel().setGraph(graph);
                frame.paint(frame.getGraphics());
                frame.getMenuPanel().update(frame.getMenuPanel().getGraphics());
            } catch (InvalidTypeException ex) {
                ex.printStackTrace();
            } catch(NullPointerException ignored){
            }
        });
    }
}
