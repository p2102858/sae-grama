package grama.gui.components.menuitems.file;

import grama.graph.Graph;
import grama.graph.exceptions.InvalidTypeException;
import grama.gui.components.frames.MainFrame;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;

public class OpenFileMenuItem extends JMenuItem {

    private final MainFrame frame;
    private final Graph graph;

    /**
     * Open another file to read the graph
     * @param frame parent frame
     * @param graph displayed graph
     */
    public OpenFileMenuItem(MainFrame frame, Graph graph) {
        this.frame = frame;
        this.graph = graph;
        setText("Ouvrir un autre fichier");
        setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
        addActionListener(e->{

            try {
                graph.readGraph(selectOpenFile().getPath());
                frame.repaint();
            } catch (InvalidTypeException ex) {
                JOptionPane.showMessageDialog(frame, "Le fichier spécifié est invalide !","Fichier invalide",JOptionPane.WARNING_MESSAGE);
                System.out.println("Invalid type found in the grama.graph text file !");
                System.out.println("Type 1 = "+ex.getType1());
                System.out.println("Type 2 = "+ex.getType2());
                System.out.println("Invalid type = "+ex.getInvalidType());
                System.out.println("Name1 = "+ex.getDepNode());
                System.out.println("Name 2 = "+ex.getArrNode());
            }
        });
    }
    public File selectOpenFile(){
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.OPEN_DIALOG);
        fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int returnVal = fileChooser.showSaveDialog(frame.getFileMenu());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile();
        }
        return null;
    }
}
