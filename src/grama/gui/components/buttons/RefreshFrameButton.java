package grama.gui.components.buttons;

import grama.gui.components.frames.MainFrame;

import javax.swing.JButton;

public class RefreshFrameButton extends JButton {
    /**
     * Refresh frame
     * @param frame parent frame
     */
    public RefreshFrameButton(MainFrame frame) {
        super();
        setText("Réinitialiser le graphe");
        addActionListener(e-> {
            frame.getGraphPanel().getNodesToHighlight().clear();
            frame.getMenuPanel().getDisplayDirectNeighbours().setSelected(false);
            frame.getGraphPanel().setCoordsRandom(frame.getGraphPanel().getNodesToDisplay(),frame);
            frame.repaint();
        });
        setFocusPainted(false);
    }
}
