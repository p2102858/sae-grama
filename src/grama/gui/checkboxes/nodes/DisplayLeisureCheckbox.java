package grama.gui.checkboxes.nodes;


import grama.gui.components.frames.MainFrame;

import javax.swing.JCheckBox;
import java.awt.Dimension;

public class DisplayLeisureCheckbox extends JCheckBox {
    private MainFrame frame;

    /**
     * Display or not the nodes with the type 'L'
     * @param frame parent frame
     */
    public DisplayLeisureCheckbox(MainFrame frame) {
        super();
        this.frame = frame;
        addActionListener(e->{
            frame.getGraphPanel().setDisplayLeisurePlaces(!frame.getGraphPanel().isDisplayLeisurePlaces());
            frame.repaint();
        });
        setText("Afficher les lieux de loisir");
        setFocusPainted(false);
        setSelected(true);
        setMinimumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setPreferredSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setMaximumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
    }
}
