package grama.gui.checkboxes.nodes;


import grama.gui.components.frames.MainFrame;

import javax.swing.JCheckBox;
import java.awt.Dimension;

public class DisplayCitiesCheckbox extends JCheckBox {

    /**
     * Display or not the nodes with the type 'V'
     * @param frame parent frame
     */
    public DisplayCitiesCheckbox(MainFrame frame) {
        super();
        addActionListener(e->{
            frame.getGraphPanel().setDisplayCities(!frame.getGraphPanel().isDisplayCities());
            frame.repaint();
        });
        setText("Afficher les villes");
        setFocusPainted(false);
        setSelected(true);
        setMinimumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setPreferredSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setMaximumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
    }
}
