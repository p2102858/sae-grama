package grama.gui.checkboxes.nodes;

import grama.gui.components.frames.MainFrame;

import javax.swing.JCheckBox;
import java.awt.Dimension;

public class DisplayRestaurantsCheckbox extends JCheckBox {
    /**
     * Display or not the nodes with the type 'R'
     * @param frame parent frame
     */
    public DisplayRestaurantsCheckbox(MainFrame frame) {
        super();
        addActionListener(e-> {
            frame.getGraphPanel().setDisplayRestaurents(!frame.getGraphPanel().isDisplayRestaurents());
            frame.repaint();
        });
        setText("Afficher les restaurants");
        setFocusPainted(false);
        setSelected(true);
        setMinimumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setPreferredSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setMaximumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
    }
}
