package grama.gui.checkboxes.edges;


import grama.gui.components.frames.MainFrame;

import javax.swing.JCheckBox;
import java.awt.Dimension;

public class DisplayDepartmentalsCheckbox extends JCheckBox {
    /**
     * Display or not edges with the type 'D'
     * @param frame parent frame
     */
    public DisplayDepartmentalsCheckbox(MainFrame frame){
        super();
        addActionListener(e->{
            frame.getGraphPanel().setDisplayDepartmentals(!frame.getGraphPanel().isDisplayDepartmentals());
            frame.repaint();
        });
        setText("Afficher les departementales");
        setFocusPainted(false);
        setSelected(true);
        setMinimumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setPreferredSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setMaximumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
    }
}
