package grama.gui.checkboxes.edges;


import grama.gui.components.frames.MainFrame;

import javax.swing.JCheckBox;
import java.awt.Dimension;

public class DisplayHighwaysCheckbox extends JCheckBox {
    /**
     * Display or not the edges with the type 'H'
     * @param frame parent frame
     */
    public DisplayHighwaysCheckbox(MainFrame frame){
        super();
        addActionListener(e->{
            frame.getGraphPanel().setDisplayHighways(!frame.getGraphPanel().isDisplayHighways());
            frame.repaint();
        });
        setText("Afficher les autoroutes");
        setFocusPainted(false);
        setSelected(true);
        setMinimumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setPreferredSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
        setMaximumSize(new Dimension((int)(frame.getWidth()*0.2) - 2* (MainFrame.GAP+10), frame.getHeight() / 40));
    }
}
