package grama;

import grama.assets.AssetGetter;
import grama.graph.Graph;
import grama.graph.exceptions.InvalidTypeException;
import grama.gui.components.frames.MainFrame;

import javax.swing.*;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args){
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        Graph graph = new Graph();
        try {
            graph.readGraph(AssetGetter.getGraphPath("Graph.csv"));
        } catch (InvalidTypeException ex){
            System.out.println("Invalid type found in the grama.graph text file !");
            System.out.println("Type 1 = "+ex.getType1());
            System.out.println("Type 2 = "+ex.getType2());
            System.out.println("Invalid type = "+ex.getInvalidType());
            System.out.println("Name1 = "+ex.getDepNode());
            System.out.println("Name 2 = "+ex.getArrNode());
            System.exit(0);
        }
        java.awt.EventQueue.invokeLater(() -> {
            MainFrame mainFrame = new MainFrame(graph,1200, 750);
            mainFrame.setVisible(true);
        });
    }
}
