package grama.interfaces;

import grama.graph.models.Node;

import java.awt.event.MouseEvent;

public interface NodeMouseListener {

    void nodeEntered(MouseEvent e, Node node);
    void nodeDragged(MouseEvent e, Node node);
    void nodeClicked(MouseEvent e, Node node);

}
