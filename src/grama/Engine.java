package grama;

import grama.graph.models.Node;
import grama.gui.components.panels.GraphDisplayPanel;
import grama.interfaces.NodeMouseListener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class Engine {

    private NodeMouseListener listener;
    private GraphDisplayPanel graphDisplayPanel;
    private Node hoveredNode;
    private List<Node> nodes;

    /**
     * Tracks the movement of the screen
     * @param graphDisplayPanel panel used to display the graph
     */
    public Engine(GraphDisplayPanel graphDisplayPanel) {
        this.graphDisplayPanel = graphDisplayPanel;
        this.nodes = new ArrayList<>(this.graphDisplayPanel.getGraph().getNodes());
        this.graphDisplayPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                for (Node node : nodes) {
                    if (x >= node.getX() && x < node.getX() + node.getDiameter() && y >= node.getY() && y < node.getY() + node.getDiameter()) {
                        listener.nodeClicked(e, node);
                        break;
                    }
                }
            }
        });
        this.graphDisplayPanel.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                for (Node node : nodes) {
                    if (x >= node.getX() && x < node.getX() + node.getDiameter() && y >= node.getY() && y < node.getY() + node.getDiameter()) {
                        listener.nodeDragged(e, node);
                    }
                }
                GraphDisplayPanel.nodesDragged = 0;
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                boolean nodeEntered = false;
                int x = e.getX();
                int y = e.getY();
                for (Node node : nodes) {
                    if (x >= node.getX() && x < node.getX() + node.getDiameter() && y >= node.getY() && y < node.getY() + node.getDiameter()) {
                        listener.nodeEntered(e, node);
                        nodeEntered = true;
                    }
                }
                if (!nodeEntered) {
                    if (hoveredNode != null) {
                        hoveredNode.setHovered(false);
                        hoveredNode = null;
                        GraphDisplayPanel.nodesHovered = 0;
                    }
                }
            }
        });
    }

    public void setListener(NodeMouseListener listener) {
        this.listener = listener;
    }

    public Node getHoveredNode() {
        return hoveredNode;
    }

    public void setHoveredNode(Node hoveredNode) {
        this.hoveredNode = hoveredNode;
    }
}
