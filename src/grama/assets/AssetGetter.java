package grama.assets;

public class AssetGetter {

    public static String getGraphPath(String fileName) {
        return AssetGetter.class.getResource(fileName).getPath();
    }
}
