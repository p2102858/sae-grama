# SAE GRAMA

This is a project developped in Java for the french IUT of computer science of Lyon

## Graph-Map analysis

This application is a tool to analyse a graph map. The graph contains various nodes representing real life places with types such as leisure places or restaurants. These nodes are connected by edges representing rodes that also have categories such as highways or departmental roads.

### Main functionnalities

Filtering the nodes and edges by types, getting the number of nodes of each type at 1, 2 or 3 distance...

